import React, { useState } from "react";
import { Data } from './data'
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Button,
    TouchableOpacity,
    Pressable
  } from "react-native";
import { FlatList } from "react-native-gesture-handler";


export default  function Home({navigation}){
    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.form}>
                    <Image
                    style={styles.searchicon}
                    source={require('./../search.png')}
                    />
                    <TextInput
                    style={styles.forminput}
                    placeholder="Search"
                    />
                </View>
                <Image
                    style={styles.banner}
                    source={require('./../banner1.jpg')}
                />
                <View style={{alignItems:'center',  height: "70%"}}>
                    <FlatList numColumns={2}
                    data={ Data }
                    renderItem={({item})=>(
                        <View style={styles.content}>
                            <Image style={styles.gambar} source={item.image}/>
                            <Text style={styles.title} >{item.title}</Text>
                            <Text>{item.harga}</Text>
                            <Button 
                            onPress={ () => navigation.navigate('DetailScreen', {
                                itemHarga: item.harga,
                                itemtitle: item.title,
                                rincian: item.desc,
                                gambar: item.image,
                              })} 
                            title="LIHAT" />
                        </View>
                    )}
                    />
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#232323",
        fontFamily : 'Roboto', 
    },
    header:{
        marginTop: 50,
        marginHorizontal: 5,
    },
    form:{
        flexDirection:"row",
        backgroundColor:"#FFFFFF",
        height: 40,
        borderRadius: 6,
    },
    forminput:{
        fontSize:20,
    },
    searchicon:{
        alignSelf: "center",
        marginHorizontal: 10,
    },
    banner:{
        margin:15,
        alignSelf:"center",
        width:"80%",
        height:"20%",
        resizeMode:'stretch',
        borderRadius:10,
    },
    content:{
        width: 160,
        height: 230,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:"#003366",
        backgroundColor: "#FFFFFF"
    },
    gambar:{
        margin:5,
        height:130,
        width:130,
        borderRadius: 5,
    },
    title:{
        fontWeight: "700"
    }
})