const Data =[
    {
        id : "1",
        harga: 250000,
        image: require("./../item1.jpg"),
        title : "La Vie Est Belle",
        desc : " La Vie Est Belle dari Lancome ini menciptakan semangat muda yang dapat mencerahkan hari-harimu",
    },
    {
        id : "2",
        harga: 275000,
        image: require("./../item2.jpg"),
        title : "Daisy Eau So Fresh",
        desc : "Buat kamu yang sangat feminin, parfum ini pasti akan menarik hatimu. Perpaduan harum bunga yang lembut",
    },
    {
        id : "3",
        harga: 305000,
        image: require("./../item3.jpg"),
        title : " Coco Mademoiselle",
        desc : "Dikenal dengan produk parfum yang selalu menakjubkan, Coco Chanel menghadirkan produk parfum Mademoiselle",
    },
    {
        id : "4",
        harga: 225000,
        image: require("./../item4.jpg"),
        title : "Gucci Bloom",
        desc : "Dikenal dengan produk parfum yang selalu menakjubkan, Coco Chanel menghadirkan produk parfum Mademoiselle",
    },
    {
        id : "5",
        harga: 265000,
        image: require("./../item5.jpg"),
        title : "Louis Vuitton Le Jour",
        desc : "Keharuman yang mewah di dapat dari campuran bunga dan sentuhan mandarine yang segar",
    },
    {
        id : "6",
        harga: 425000,
        image: require("./../item6.jpg"),
        title : "Miu Miu L’Eau",
        desc : "Menyukai harum yang unik dan sedikit tajam? Parfum yang satu ini bisa kamu coba lho.",
    },
]
export {Data};