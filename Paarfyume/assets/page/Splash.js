import React,{useEffect} from "react";
import { View ,Image} from "react-native";

const SplashScreen = ({navigation}) =>{
    useEffect(()=>{
        setTimeout(()=>{
            navigation.navigate('LoginScreen')
        },3000);
    },[]);
    return(
        <View style={{backgroundColor:'#232323', flex:1, justifyContent:'center',alignItems:'center'}}>
            <Image
            source={require('./../iconsplash.png')}
            />
            <Image
            source={require('./../iconsplash2.png')}
            />
        </View>
    );
};

export default SplashScreen;