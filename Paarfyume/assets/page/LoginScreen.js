import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import React, { useState } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Button,
    TouchableOpacity,
    Pressable
  } from "react-native";
import { Alert } from "react-native-web";

export default function Login({navigation}){
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const register = () => {
        navigation.navigate('RegisterScreen');
    };
    const submit = () => {
        const Data = {
            email,
            password,
        }
        const auth = getAuth();
        signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            // ...
            navigation.navigate('HomeScreen')
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.error(errorCode,errorMessage)
        });

    }
    return(
        <View style={styles.container}>
            <Image
            style={styles.gambar}
            source={require('./../iconsplash.png')}
            />
            <View style={styles.content}>
                <Text style={styles.textatas}>SIGN IN</Text>
                <Text style={styles.textatas}>TO CONTINUE</Text>
            </View>
            <View style={styles.contentinput}>
            <TextInput
                style={styles.forminput}
                placeholder="Masukkan Email"
                value={email}
                onChangeText={(value) => setEmail(value)}
                />
                <TextInput
                style={styles.forminput}
                secureTextEntry={true}
                placeholder="Masukkan Password"
                value={password}
                onChangeText={(value) => setPassword(value)}
                />
            </View>
            <Pressable style={styles.button} onPress={submit}>
            <Text style={styles.text}>Log in</Text>
            </Pressable>
            <View style={styles.containerregis}>
                <Text style={styles.textregis}>Don't have an account?</Text>
                <Text style={styles.textregistombol} onPress={register} > Sign Up</Text>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#232323",
        fontFamily : 'Roboto', 
    },
    gambar: {
        width: 100,
        height: 100,
        marginTop: 135,
        alignSelf: 'center',
    },
    content : {
        marginTop:15,
        marginHorizontal:15,
    },
    contentinput : {
        marginTop:30,
        marginHorizontal:40,
    },
    textatas : {
        color: '#FFFFFF',
        fontSize: 24,
        fontWeight: "600",
        alignSelf: 'center'
    },
    forminput :{
        backgroundColor: '#FFFFFF',
        borderRadius: 6,
        height:40,
        paddingHorizontal:15,
        fontSize:16,
        marginVertical:8,
    },
    button: {
        marginTop:30,
        alignItems: 'center',
        justifyContent: 'center',
        height:32,
        width: 130,
        borderRadius: 6,
        elevation: 3,
        backgroundColor: 'white',
        alignSelf: 'center'
    },
    text: {
        fontSize: 18,
        lineHeight: 21,
        fontWeight: '600',
        letterSpacing: 0.25,
        color: '#232323',
    },
    containerregis:{
        margin:20,
        justifyContent:"center",
        flexDirection:"row",
        alignItems:"center",
    },
    textregis:{
        color:"white",
    },
    textregistombol:{
        color:"white",
        fontWeight:"700"
    },
})