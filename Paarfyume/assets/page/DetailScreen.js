import React, { useState } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Button,
    TouchableOpacity,
    Pressable
  } from "react-native";

  export default function Detail({route, navigation}){
    const { itemHarga } = route.params;
    const { itemtitle } = route.params;
    const { rincian } = route.params;
    const { gambar } = route.params;
      return(
        <View style={styles.container}>
            <Image source={gambar } style={styles.gambar}/>
            <Text style={styles.title}>{itemtitle}</Text>
            <Text style={styles.harga}>Rp {itemHarga}</Text>
            <Text style={styles.title}>{rincian}</Text>
            <View style={styles.footer} >
                <Button title="Masukkan Keranjang"/>
            </View>
            
        </View>

      )
  }

  const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#232323",
        fontFamily : 'Roboto', 
    },
    gambar:{
        alignSelf:"center",
        width:"100%",
        height:"45%",
        resizeMode:'stretch',
    },
    harga:{
        fontSize:24,
        color:"#FFFFFF",
        fontWeight:"700"
    },
    title:{
        fontSize:18,
        color:"#FFFFFF",
        fontWeight:"600"
    },
    footer: {
        marginTop: 50,
        bottom:0,
    }
  })