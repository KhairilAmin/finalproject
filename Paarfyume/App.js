import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import SplashScreen from './assets/page/Splash';
import LoginScreen from './assets/page/LoginScreen';
import HomeScreen from './assets/page/HomeScreen';
import RegisterScreen from './assets/page/RegisterScreen'
import DetailScreen from './assets/page/DetailScreen'
import { getApps, initializeApp } from 'firebase/app';
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { useEffect, useState } from 'react';

const firebaseConfig = {
  apiKey: "AIzaSyAntF4V5s1UL1FqNKDmNv0Oy4wOcUGQ5aQ",
  authDomain: "paarfyume.firebaseapp.com",
  projectId: "paarfyume",
  storageBucket: "paarfyume.appspot.com",
  messagingSenderId: "449678665358",
  appId: "1:449678665358:web:3922351d13106f6cdf4bdf",
  measurementId: "G-EJJH791J0X"
};

if (!getApps().length){
  const app = initializeApp(firebaseConfig);
}
console.log(getApps());


const Stack = createStackNavigator();

export default function App() {
const [isSignedIn, setisSignedIn] = useState(false)
useEffect(() =>{
  const auth = getAuth();
  const unsubscribe = onAuthStateChanged(auth, (user) => {
  if (user) {
    // User is signed in, see docs for a list of available properties
    // https://firebase.google.com/docs/reference/js/firebase.User
    const uid = user.uid;
    setisSignedIn(true);
    // ...
  } else {
    // User is signed out
    // ...
    setisSignedIn(false);
  }
});
  return unsubscribe;
},[])
  return (
    <NavigationContainer>
        <Stack.Navigator >
            <Stack.Screen name='Splash' component={SplashScreen} options={{ headerShown: false }} />
            <Stack.Screen name='LoginScreen' component={LoginScreen} options={{ headerShown: false }} />
            <Stack.Screen name='RegisterScreen' component={RegisterScreen} options={{ headerShown: false }} />
            <Stack.Screen name='HomeScreen' component={HomeScreen} options={{ headerShown: false }} /> 
            <Stack.Screen name='DetailScreen' component={DetailScreen} options={{ headerTitle: 'Detail Barang'}} />
        </Stack.Navigator>
      </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
